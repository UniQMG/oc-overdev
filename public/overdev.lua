local internet = require('internet')
local process = require('process')
local fs = require('filesystem')
local shell = require('shell')
local thread = require('thread')

local dir = fs.concat(shell.getWorkingDirectory(), 'sync')
local host = '192.168.0.10:5471'

fs.remove(dir)
fs.makeDirectory(dir)

print("\27[32m[OverDev] Starting up. Target dir: " .. dir .. "\27[0m")

local worker = nil
local connection = nil
local onMessage = {
  restart = function()
    -- print("Do restart")
    local path = fs.concat(dir, "index.lua")
    print("\27[32m[OverDev] Starting " .. path .. "\27[0m")

    local oldPWD = shell.getWorkingDirectory()
    shell.setWorkingDirectory(dir)

    if worker ~= nil then
      worker:kill()
    end

    worker = thread.create(function()
      local status, err = shell.execute(path)
      shell.setWorkingDirectory(oldPWD)

      if not status then
        print("\27[31m[OverDev] Error " .. err .. "\27[0m")
      end
    end)

    -- print("\27[32m[OverDev] Ready\27[0m")
  end,
  fetch = function(path)
    print("\27[32m[OverDev] Fetching " .. path .. "\27[0m")

    local relpath = fs.concat(dir, path)
    local dir = fs.path(relpath)
    if dir ~= "" then
      fs.makeDirectory(dir)
    end

    local ok, result = pcall(function()
      return internet.request('http://' .. host .. '/files/' .. path)
    end)
    if not ok then
      print("\27[31m[OverDev] Failed to fetch: " .. result)
      return
    end
    if result then
      local file, fErr = io.open(relpath, 'w')
      if not file then
        print("\27[31m[OverDev] Failed to open file: " .. fErr .. "\27[0m")
        return
      end
      for chunk in result do
        file:write(chunk)
      end
      file:close()
    else
      print("\27[31m[OverDev] Request failed: " .. response .. "\27[0m")
    end
  end,
  remove = function(relpath)
    print("\27[32m[OverDev] Unlinking " .. relpath .. "\27[0m")
    local path = fs.concat(dir, relpath)
    fs.remove(path)
  end,
  reconnect = function()
    if connection then
      os.sleep(1)
      -- Ends the program for some reason
      -- connection:kill()
      -- connection = nil
    end

    connection = thread.create(function()
      url = 'http://' .. host
      print("\27[32m[OverDev] Connecting to " .. url .. "\27[0m")

      local ok, result = pcall(function()
        return internet.request(url)
      end)
      if not ok then
        print("\27[31m[OverDev] Connection failed: " .. result)
        os.exit(1)
      end
      for chunk in result do
        -- print("\27[31m[OverDev] Chunk: " .. string.gsub(chunk, "\n", "\\n") .. "\27[0m")
        processChunk(chunk)
      end
      print("\27[32m[OverDev] No further chunks");
    end)
  end
}

function processMessage(msg)
  if msg == "" then return end
  -- print("\27[36mMessage: " .. msg .. "\27[0m")

  local iter = string.gmatch(msg, "[^%s]+")
  local cmd = iter()
  local callback = onMessage[cmd]

  if callback == nil then
    print("\27[31m[OverDev] Error: unknown callback " .. cmd .. "\27[0m")
    -- os.exit(1)
  else
    callback(iter(), iter(), iter())
  end
end

local buffer = ""
function processChunk(chunk)
  local index = string.find(chunk, '\n')
  if index == nil then
    buffer = buffer .. chunk
  else
    processMessage(buffer .. string.sub(chunk, 1, index-1))
    buffer = ""
    processChunk(string.sub(chunk, index+1))
  end
end

onMessage.reconnect()
