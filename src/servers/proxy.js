const { getSocket } = require('./forwarder');
const Net = require('net');

/*
  The Proxy server is responsible for recieving the actual HTTP requests
  and then passing it along to the Forwarder's client(s).
*/

const server = new Net.Server();
let requests = 0;

server.on('connection', socket => {
  console.log('Incoming connection');
  socket.setEncoding('utf8');

  const requestId = ++requests;
  const forwarder = getSocket();
  if (!forwarder) {
    socket.write('HTTP/1.1 502 OK\n\nBad gateway\n')
    socket.end();
    return;
  }
  forwarder.send(JSON.stringify({ op: 'open', requestId }));
  socket.on('end', () => {
    if (forwarder.readyState != 1) return;
    forwarder.send(JSON.stringify({ op: 'close', requestId }));
  });
  socket.on('error', ex => {
    console.warn(ex);
    socket.end();
  });
  // Browser -> Minecraft
  socket.cork();
  socket.on('data', chunk => {
    if (forwarder.readyState != 1) return;
    forwarder.send(JSON.stringify({
      op: 'data',
      requestId,
      data: chunk.replace(/\r/g, '')
    }));
  });
  // Minecraft -> Browser
  forwarder.on('message', msg => {
    let { op, requestId: rid, data } = JSON.parse(msg);
    if (requestId != rid) return;
    switch (op) {
      case 'opened': socket.uncork();    break;
      case 'closed': socket.end();       break;
      case   'data': socket.write(data); break;
    }
  });
})

function start(port) {
  server.listen(port, () => {
    console.log(`Proxy server listening on port ${port}`);
  });
}
module.exports = {start};
