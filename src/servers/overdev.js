const express = require('express');
const chokidar = require('chokidar');
const path = require('path');
const _ = require('lodash');
const app = express();

/*
  The Overdev server is a deployment tool that functions like a remote nodemon.
  Whenever a file is saved locally, all computers running the client script will
  re-fetch it and restart their program. See overdev.lua.
*/


const dir = 'sync';

function testSafety(relpath) {
  if (/\s/.test(relpath) != 0)
    throw new Error('File paths with whitespace (/\\s/) are not supported');
}

app.get('/', async (req, res) => {
  const update = _.debounce(() => res.write('restart\n'), 100);

  const fetch = function(relpath) {
    testSafety(relpath);
    res.write('fetch ' + path.relative(dir, relpath) + '\n');
    update();
  }

  const remove = function(relpath) {
    testSafety(relpath);
    res.write('remove ' + path.relative(dir, relpath) + '\n');
    update();
  }

  const exit = function() {
    console.log('exit handler');
    res.write('reconnect\n');
    res.end();
  }

  let watcher = chokidar
    .watch(dir)
    .on('add', fetch)
    .on('change', fetch)
    .on('unlink', remove);

  process.on('exit', exit);
  await new Promise(res => req.on('close', res));
  watcher.close();
  process.removeListener('exit', exit);
});
app.use(express.static('public'));
app.use('/files', express.static(dir));

process.on('SIGINT', () => process.exit());
process.on('SIGUSR2', () => process.exit());

function start(port) {
  app.listen(port, () => {
    console.log(`Overdev server listening on port ${port}`);
  });
}
module.exports = {start};
