const express = require('express');
const ws = require('express-ws');
const app = express();

/*
  The Forwarder server is responsible for acting as a request gateway. Clients
  open a persistent tcp socket to the gateway which sends newline-delimited
  json packets back and forth.

  The bulk of the actual IO logic happens in proxy.js.
*/

ws(app);
app.use(express.static('public'));

let socket = null;
let requests = 0;
let ids = 0
app.ws('/socket', (newSocket, req) => {
  let id = ++ids;
  socket = newSocket;
  console.log(`Socket #${id} connected from ${req.connection.remoteAddress}`);
  try {
    setTimeout(() => {
      return;
      let requestId = ++requests;
      let reqstr = (
        "GET /index.html HTTP/1.1\n" +
        "Host: www.example.com\n" +
        "User-Agent: Me\n" +
        "Accept: text/html, */*\n" +
        "Accept-language: en-us\n" +
        "Accept-charset: ISO-8859-1,utf-8\n" +
        "Connection: keep-alive\n" +
        "\n"
      );
      socket.send(JSON.stringify({ op: 'open', requestId }));
      socket.send(JSON.stringify({ op: 'data', requestId, data: reqstr }));
      socket.send(JSON.stringify({ op: 'close', requestId }));
    }, 100)

    socket.on('message', msg => {
      try {
        console.log('websocket>', JSON.parse(msg));
      } catch(ex) {
        console.log('websocket>', msg);
      }
    });
    socket.on('close', () => {
      console.log(`Socket #${id} closed`);
    });
  } catch(ex) {
    console.error(ex);
  }
});

function start(port) {
  app.listen(port, () => {
    console.log(`Forwarder server listening on port ${port}`);
  });
}
module.exports = { start, getSocket: () => socket };
