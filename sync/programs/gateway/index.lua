local websocket = require('lib/websocket_client')
local json = (loadfile "lib/json.lua")()
local component = require('component')
local computer = require('computer')
local minitel = require('minitel')
local thread = require('thread')
local term = require('term')
local text = require('text')
local l5 = require('lib/layer5')


print("Gateway initializing...")

local connections = {} -- Internal connections to other servers
local ops = {} -- Callbacks for external websocket events

local ws = websocket.create(function(event, arg)
  log('[ws event] ' .. event .. ' -> ' .. arg)
  local table = json:decode(arg)
  ops[table.op](table.requestId, table.data)
  log('Done')
end)

function log(string)
  -- Todo: dump this to disk instead, or build some sort of logger
  -- ws:send(json:encode({ op = 'debug', message = string }))
end

function sendError(id, code, body)
  ws:send(json:encode({
    op = 'data',
    requestId = id,
    data = (
      "HTTP/1.1 " .. code .. " OK\n" ..
      "Server: Gateway\n" ..
      "Content-Type: text/html;charset=UTF-8\n" ..
      "Content-Length: " .. (string.len(body)+1) .. "\n" ..
      "\n" ..
      body ..
      "\n"
    )
  }))
end

function startConnection(id, host)
  log("Starting connection to " .. host)
  local conn = connections[id]
  conn.host = host

  thread.create(function()
    local stream, err = l5.open(host, 1000)

    if not stream then
      sendError(id, 504, "Failed to open stream: " .. err)
      terminateConnection(id)
    else
      log("Connection opened")
      conn.stream = stream
      ws:send(json:encode({
        op = 'opened',
        requestId = id
      }))
      -- Empty buffered data
      conn.stream:write(conn.buffer)
      conn.buffer = ""

      -- Begin reading in data
      while not conn.finished do
        local data = conn.stream:read("*a")
        conn.totalData = conn.totalData + string.len(data)

        if string.len(data) > 0 then
          conn.lastActive = computer.uptime()
          ws:send(json:encode({
            op = 'data',
            requestId = id,
            data = data
          }))
        end

        if conn.stream.state == 'closed' then
          terminateConnection(id)
        end
        os.sleep(string.len(data) == 0 and 1 or 0.1)
      end
    end
  end)
end

function terminateConnection(id)
  local conn = connections[id]
  if conn.finished then return end
  conn.finished = true

  if conn.stream then
    conn.stream:close()
  end

  conn.lastActive = computer.uptime()
  ws:send(json:encode({
    op = 'closed',
    requestId = id,
    error = 'closed'
  }))
end

function updateConnection(id)
  local conn = connections[id]
  local idleTime = computer.uptime() - conn.lastActive

  -- Clear out finished connections with a delay
  if conn.finished then--and idleTime > 10 then
    connections[id] = nil
    return
  end

  -- Abort long running connections
  if idleTime > 30 then
    -- If data has been sent, we can't send a new response
    -- Only thing to do is just cut the connection at this point
    if conn.totalData == 0 then
      if conn.host then
        sendError(id, 504, "Gateway timeout")
      else
        sendError(id, 408, "Request timeout")
      end
    end
    terminateConnection(id)
  end
end

ops.open = function(id)
  if connections[id] then
    ws:send(json:encode({
      op = 'error',
      requestId = id,
      error = 'Connection #' .. id .. 'already exists'
    }))
    return
  end
  log('New request: #' .. id)
  connections[id] = {
    stream = nil,
    lastActive = computer.uptime(),
    openedAt = computer.uptime(),
    totalData = 0,
    buffer = "",
    host = nil,
    finished = false
  }
end
ops.close = function(id)
  terminateConnection(id)
end
ops.data = function(id, data) -- Data from internet
  local conn = connections[id]
  if (not conn) or conn.finished then return end

  if conn.stream then
    conn.stream:write(data)
    return
  end

  conn.buffer = conn.buffer .. data
  local host = conn.buffer:match("\nHost: ([A-Za-z0-9%.%-:]+)\n")

  if host then
    -- Extract leftmost subdomain to use as the actual hostname
    -- so that this can be hosted on any real domain.
    host = host:match("^([A-Za-z0-9%-]+)")
    if string.len(host) > 8 then
      sendError(id, 400, "Leftmost host header subdomain too long (max 8 chars)")
      terminateConnection(id)
      return
    end
    startConnection(id, host)
    return
  end

  if newline then
    -- No host header specified!
    sendError(id, 400, "Please include a Host header")
    terminateConnection(id)
    return
  end

  if string.len(conn.buffer) > 4096 then
    sendError(id, 400, "Headers too long")
    terminateConnection(id)
  end

  -- wait for host header
end

local ip = "192.168.0.10"
print("Connecting to ws://" .. ip .. ":5472/socket")
ws:connect(ip, 5472, "/socket");

-- Connection updater loop
thread.create(function()
  while true do
    for id, conn in pairs(connections) do
      updateConnection(id)
    end
    os.sleep(1)
  end
end)

-- Screen output loop
local _, yres = component.gpu.getResolution()
component.gpu.setResolution(50, yres)

local clrl = "\27[0K" -- Clear to end of line
local endl = "\27[0K\n" -- Clear to end of line then new line
while true do
  term.setCursor(1,1)

  term.write("[ Gateway ]")
  local w, h, x, y, rx, ry = term.getViewport()
  local str = string.format("Up %ss", math.floor(computer.uptime()))
  term.write(string.rep(" ", w - (string.len(str) + rx)))
  term.write(str .. endl)

  term.write('┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓' .. endl)
  term.write('┃ Conn     State    Host     Idle     Data      ┃' .. endl)

  local active = 0
  for id, conn in pairs(connections) do
    active = active + 1

    local formatted = text.detab(string.format(
      "%s\t%s\t%s\t%ss\t%sB\t",
      id,
      conn.finished
        and 'ended'
        or conn.stream
          and conn.stream.state
          or 'waiting',
      conn.host or '-',
      math.floor(computer.uptime() - conn.lastActive),
      conn.totalData
    ), 9)
    term.write("┃ " .. formatted .. " ┃" .. endl)
  end

  local w, h, x, y, rx, ry = term.getViewport()
  while ry < h-1 do
    term.write('┃ -        -        -        -        -         ┃' .. endl)
    ry = ry + 1
  end

  term.write('┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛' .. endl)
  term.write('Active connections: ' .. active)

  local total = computer.totalMemory()
  local free = computer.freeMemory()
  local mem = math.floor(((total - free) / total)*100)
  local str = 'Memory usage: ' .. mem .. '%'

  local w, h, x, y, rx, ry = term.getViewport()
  term.write(string.rep(" ", w - (string.len(str) + rx)))
  term.write(str)
  os.sleep(0.25)
end
