local minitel = require('minitel')
local thread = require('thread')
local event = require('event')
local l5 = require('lib/layer5')
-- require('term').clear()

require('component').gpu.setResolution(50, 25)

local stats = require('programs/refinery/stats')
function trim(s)
  return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end
function uptime()
  return require('computer').uptime()
end

print "Listening for connections"
l5.listen(1000, function(socket)
  print "New connection"

  local timeout = 0
  local errored = false
  function readline()
    while true do
      local result = socket:read()
      if result ~= nil then
        timeout = 0
        return trim(result)
      end
      os.sleep(0.1)
      timeout = timeout+0.1
      if timeout > 15 then
        socket:write("HTTP/1.1 408 OK\n\n")
        socket:close()
        print("Timeout")
        errored = true
        return ""
      end
    end
  end

  local firstline = readline()
  -- print("Got first line", firstline)
  local header = string.gmatch(firstline, "[^%s]+")
  local verb = header()
  local path = header()
  local vers = header()
  print("verb", verb)
  print("path", path)
  print("vers", vers)

  local headers = {}
  while true do
    local line = readline()
    -- print("Got line", line, "(Length: " .. string.len(line) .. ")")
    if line == "" or line == "\r" then
      break
    end
    local _, _, header, value = string.find(line, "(.-):(.+)")
    value = trim(value)
    print(header .. " = " .. value)
    headers[header] = value
  end

  if errored then
    print("Error while parsing headers, aborting")
    return
  end

  print("Done parsing headers, writing response")


  socket:write("HTTP/1.1 200 OK\n")
  socket:write("Server: webserver\n")
  socket:write("Content-Type: text/html;charset=UTF-8\n")
  local start = uptime()
  local res = "<pre>" .. stats() .. "</pre>"
  res = res .. '<meta http-equiv="refresh" content="1" />'
  res = res .. "Generated in " .. (uptime() - start) .. " seconds\n"
  socket:write("Content-Length: " .. string.len(res) .. "\n")
  socket:write("\n")
  socket:write(res .. "\n")
  socket:close()
  print("Response sent")
end)
