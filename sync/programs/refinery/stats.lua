local component = require('component')
local computer = require('computer')

local refinery = component.refinery
local compressor = component.thermal_compressor
local battery = component.capacitor_bank
local tanks = component.list('tank_controller')

return function()
  local temp = refinery.getTemperature() - 273.15
  local pres = compressor.getPressure()

  local power = battery.getEnergyStored()
  local maxPower = battery.getMaxEnergyStored()
  local powerin = battery.getAverageInputPerTick()
  local powernet = battery.getAverageChangePerTick()

  local total = computer.totalMemory()
  local free = computer.freeMemory()

  local str = ""

  str = str .. "Server\n"
  str = str .. "Uptime: " .. computer.uptime() .. "\n"
  str = str .. "Mem: " .. (total - free) .. " / " .. total .. "\n"
  -- str = str .. "Disk usage: " .. require('filesystem').spaceUsed() .. " bytes\n"

  str = str .. "\nRefinery\n"
  str = str .. "Temperature: " .. math.floor(temp * 10)/10 .. "C\n"
  str = str .. "Vortex tube pressure: " .. math.floor(pres * 100)/100 .. "bar\n"
  str = str .. "Battery: " .. power .. " / " .. maxPower .. " (" .. (powernet >= 0 and "+" or "") .. math.floor(powernet) .. "/t, in: " .. math.floor(powerin) .. "/t)\n"

  for address in pairs(tanks) do
    local comp = component.proxy(address)
    for side = 0, 5 do
      fluid = comp.getFluidInTank(side)
      for i = 1, fluid.n do
        str = str .. "Found fluid " .. (fluid[i].name or "<none>") .. " (" .. fluid[i].amount .. " / " .. fluid[i].capacity .. ")\n"
      end
    end
  end

  return str
end
