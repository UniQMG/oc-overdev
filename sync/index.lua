local fs = require('filesystem')
local hs = fs.open('/etc/hostname')
local hostname = hs:read(1000)
hs:close()

print("Running on host " .. hostname)
dofile('programs/' .. hostname .. '/index.lua')
print("Goodbye")
