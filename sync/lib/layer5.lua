-- Custom reimplementation of part of
-- https://github.com/ShadowKatStudios/OC-Minitel/blob/master/OpenOS/usr/lib/minitel.lua
local event = require('event')
local net = require('minitel')

local function cwrite(self,data)
  if self.state == "open" then
    if not net.send(self.addr,self.port,data) then
      self:close()
      return false, "timed out"
    end
  end
end
local function cread(self,length)
  length = length or "\n"
  local rdata = ""
  if type(length) == "number" then
    rdata = self.rbuffer:sub(1,length)
    self.rbuffer = self.rbuffer:sub(length+1)
    return rdata
  elseif type(length) == "string" then
    if length:sub(1,2) == "*a" then
      rdata = self.rbuffer
      self.rbuffer = ""
      return rdata
    elseif length:len() == 1 then
      local pre, post = self.rbuffer:match("(.-)"..length.."(.*)")
      if pre and post then
        self.rbuffer = post
        return pre
      end
      return nil
    end
  end
end

local function socket(addr,port,sclose)
  local conn = {}
  conn.addr,conn.port = addr,tonumber(port)
  conn.rbuffer = ""
  conn.write = cwrite
  conn.read = cread
  conn.state = "open"
  conn.sclose = sclose
  local function listener(_,f,p,d)
    if f == conn.addr and p == conn.port then
      if d == sclose then
        conn:close()
      else
        conn.rbuffer = conn.rbuffer .. d
      end
    end
  end
  event.listen("net_msg",listener)
  function conn.close(self)
    event.ignore("net_msg",listener)
    conn.state = "closed"
    net.rsend(addr,port,sclose)
  end
  return conn
end

local layer5 = {}
function layer5.open(to,port)
  if not net.rsend(to, port, "openstream") then
    return false, "no ack from host"
  end

  -- print("PF#1", net.streamdelay)
  local name, from, rport, data = event.pullFiltered(
    net.streamdelay,
    function(name, from, rport, data)
      return name == "net_msg" and to == from and rport == port
    end
  )
  if name == nil then
    return nil, "timed out"
  end
  data = tonumber(data)

  -- print("PF#2", net.streamdelay, "lf", data)
  local name, from, nport, sclose = event.pullFiltered(
    function(name, from, nport, sclose)
      if name ~= "net_msg" then return end
      -- print("A", name, "net_msg")
      -- print("B", from, to)
      -- print("C", nport, data)
      return name == "net_msg" and from == to and nport == data
    end
  )
  if name == nil then
    return nil, "timed out"
  end

  -- print("Done")
  return socket(to, data, sclose)
end

-- This is blocking because of some weirdness related to how
-- minitel handles multiple connections, events, and threading.
-- I'm kinda confused by it and need to dig in deeper, but for
-- now its safer to just make it block.
local thread = require('thread')
function layer5.listen(port, listener)
  local queued = {}

  event.listen("net_msg", function(_, from, rport, data)
    table.insert(queued, function()
      return _, from, rport, data
    end)
  end)

  while true do
    if #queued > 0 then
      for key, provider in pairs(queued) do
        local _, from, rport, data = provider()
        if rport == port and data == "openstream" then
          local nport = math.random(net.minport,net.maxport)
          local sclose = net.genPacketID()
          net.rsend(from,rport,tostring(nport))
          net.rsend(from,nport,sclose)
          pcall(function()
            listener(socket(from,nport,sclose))
          end)
        end
      end
      queued = {}
    end
    os.sleep(0.2)
  end
end

return layer5
